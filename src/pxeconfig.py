# set ts=4, sw=4
#
# Author: Bas van der Vlies <bas.vandervlies@surf.nl>
# Date  : 16 February 2002
#
# Tester: Walter de Jong <walter.dejong@surf.nl>
#
# Copyright (C) 2021
#
# This file is part of the pxeconfig utils
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
#
"""
Usage: pxeconfig [-f,--filename <name>] <hosts|mac_addresses>

Specifying hosts or mac addresses:
   To specify a range use the [] to indicate a range,
   a couple of examples:

   The first five nodes of rack 16
    - gb-r16n[1-5]

   The first five nodes and node 12 and 18 of rack 16 to 20
      - gb-r[16-20]n[1-5,12,18]

   The first five nodes de in rack 16 with padding enabled
    - gb-r[16]n[01-5]

   Host with mac address 00:19:b9:de:21:47 and first five node of rack 15
    - 00:19:b9:de:21:47 gb-r15n[1-5]

The ranges ([]) are not only limited to numbers, letters can also be used.

With this program you can configure which PXE configuration file a node
will use when it boots.

See following link for usage and examples:
 - https://subtrac.sara.nl/oss/pxeconfig/wiki/PxeUsage
"""

import sys, os
import glob
import getopt
import socket
#import ConfigParser
import re

# import from the sara_python_modules
import AdvancedParser
from pxe_global import *

# Constants
#
PXE_CONF_DIR  = '/tftpboot/pxelinux.cfg'
UEFI_CONF_DIR = '/tftpboot/uefi'
NETWORK       = 'network'
START         = 'start'
END           = 'end'
VERSION       = '5.0.0'

def select_configfile(opts):
    """
    Let user choose which pxeconfig file to use.
    """

    os.chdir(opts.CONF_DIR)

    if opts.UEFI_MODE:
        default_boot_file = 'grub.cfg'
        glob_match = 'grub.cfg.*'
    else:
        default_boot_file = 'default'
        glob_match = 'default.*'

    # Try to determine to which file the default symlink points, and
    # if it exists, remove it from the list.
    #
    try:
        default_boot_file = os.readlink('default_boot_file')
    except OSError:
        default_boot_file = None
        pass

    files = glob.glob(glob_match)
    if not files:
        error =  'There are no pxe config files starting with: {}'.format(glob_match)
        raise PxeConfig(error)

    if default_boot_file:
        files.remove(default_boot_file)

    # sort the files
    #
    files.sort()

    print('Which pxe config file must we use: ?')
    i = 1
    for file in files:
        print("{:d} : {}".format(i,file))
        i = i + 1

    while 1:
        index = input('Select a number: ')
        try:
            index = int(index)
        except ValueError:
            index = len(files) + 1

        # Is the user smart enough to select
        # the right value??
        #
        if 0 < index <= len(files):
            break

    return files[index-1]


def manage_links(opts, haddr, ip_addr):
    """
    Create the links in the CONF_DIR,
    list : A list containing: network hex address, config file,
           start number, end number
    """
    if opts.VERBOSE:
        print('manage_links({})'.format(haddr))

    if opts.UEFI_MODE:
        boot_file =  "grub.cfg-{}".format(haddr)
    else:
        boot_file =  haddr

    os.chdir(opts.CONF_DIR)

    if opts.REMOVE:
        if opts.DEBUG or opts.DRY_RUN or opts.VERBOSE:
            print('removing {}/{}'.format(opts.CONF_DIR, boot_file))

            if opts.SCRIPT_HOOK_REMOVE and ip_addr:
                print('Executing client script hook remove : {} with arg: {}'.format(opts.SCRIPT_HOOK_REMOVE, ip_addr))

        if not opts.DRY_RUN:
            if os.path.exists(boot_file):
                os.unlink(boot_file)

            if opts.SCRIPT_HOOK_REMOVE and ip_addr:
                cmd = '{} {}'.format(opts.SCRIPT_HOOK_REMOVE, ip_addr)
                run_command(opts, cmd)

    else:

        if opts.DEBUG or opts.DRY_RUN or opts.VERBOSE:
            print('linking {} to {}'.format(boot_file, opts.filename))

            if opts.SCRIPT_HOOK_ADD and ip_addr:
                print('Executing client script hook add : {} with arg: {}'.format(opts.SCRIPT_HOOK_ADD, ip_addr))

        if not opts.DRY_RUN:
            if os.path.exists(boot_file):
                os.unlink(boot_file)
            os.symlink(opts.filename, boot_file)

            if opts.SCRIPT_HOOK_ADD and ip_addr:
                cmd = '{} {}'.format(opts.SCRIPT_HOOK_ADD, ip_addr)
                run_command(opts, cmd)

def net_2_hex(net, opts):
    """
    This function checks if the give network is a Class C-network and will
    convert the network address to a hex address if true.
    """
    if opts.DEBUG:
        str = 'net_2_hex : {}'.format(net)
        print(str)

    d = net.split('.')
    if len(d) != 3:
        error = '{} is not a valid  C-class network address'.format(net)
        raise PxeConfig(error)

    # Check if we have valid network values
    r = ''
    for i in d:
        r = '{}{:02X}'.format(r, int(i))

    if opts.DEBUG:
        print('C-network converted to hex: ', r)

    return r

def host_2_hex(host, opts):
    """
    Convert hostname(s) to a net address that can be handled by manage_links function
    """
    if opts.DEBUG:
        str = 'host_2_hex: {}'.format(host)
        print(str)

    try:
        addr = socket.gethostbyname(host)
    except socket.error as detail:
        error =  '{} not an valid hostname: {}'.format(host,detail)
        raise PxeConfig(error)

    net = addr.split('.')
    cnet = '.'.join(net[0:3])


    haddr = '{}{:02X}'.format(net_2_hex(cnet, opts), int(net[3]))
    manage_links(opts, haddr, addr)


def mac_2_hex(mac_addr, opts):
    """
    Convert mac address to pxeconfig address
    """
    if opts.DEBUG:
        str = 'mac_2_hex: {}'.format(mac_addr)
        print(mac_addr)

    haddr = '01-{}'.format(mac_addr.replace(':', '-').lower())
    manage_links(opts, haddr, None)

def add_options(p):
    """
    add the default opts
    """
    p.set_defaults(
        DEBUG   = False,
        DRY_RUN = False,
        REMOVE  = False,
        SCRIPT_HOOK_ADD = False,
        SCRIPT_HOOK_REMOVE = False,
        SKIP_HOSTNAME_ERRORS = False,
        UEFI_MODE = False,
        VERBOSE = False,
        VERSION  = False,
    )

    p.add_option('-d', '--debug',
        action = 'store_true',
        dest   = 'DEBUG',
        help   = 'Toggle debug mode (default : False)'
    )

    p.add_option('-f', '--filename',
        action = 'store',
        dest   = 'filename',
        help   = 'Specifies which PXE filename must be use'
    )

    p.add_option('-n', '--dry-run',
        action = 'store_true',
        dest   = 'DRY_RUN',
        help   = 'Do not execute any command (default : False)'
    )

    p.add_option('-r', '--remove',
        action = 'store_true',
        dest   = 'REMOVE',
        help   = 'Removes the PXE filename for a host(s)'
    )

    p.add_option('-u', '--uefi',
        action = 'store_true',
        dest   = 'UEFI_MODE',
        help   = 'switch to UEFI mode setup (default: False)'
    )

    p.add_option('-v', '--verbose',
        action = 'store_true',
        dest   = 'VERBOSE',
        help   = 'Make the program more verbose (default: False)'
    )

    p.add_option('-H', '--skip-hostname-lookup-error',
        action = 'store_true',
        dest   = 'SKIP_HOSTNAME_LOOKUP_ERROR',
        help   = 'When hostname lookup fails, skip it (default: False)'
    )

    p.add_option('-V', '--version',
        action = 'store_true',
        dest   = 'VERSION',
        help   = 'Print the program version number and exit'
    )

def run(hosts, opts, f_config):
    """
    Make use of sara advance parser module. It can handle regex in hostnames
    """
    # Only check if we have specified an pxeconfig file if we did not
    # specify the REMOVE option
    #
    if not opts.REMOVE:
        if opts.filename:
            if not os.path.isfile(os.path.join(opts.CONF_DIR, opts.filename)):
                error =  '{}: Filename does not exist'.format(opts.filename)
                raise PxeConfig(error)
        else:
            opts.filename = select_configfile(opts)

    ##
    # Are the hosts wiht only mac addresses defined in the configuration file
    # or specified on the command line
    #
    mac_addr_re = re.compile('([a-fA-F0-9]{2}[:|\-]?){6}')

    for host in hosts:
        if host in f_config.sections():
            mac_addr = f_config.get(host, 'mac_address')
            mac_2_hex(mac_addr, opts)

        elif mac_addr_re.search(host):
            mac_2_hex(host, opts)
        else:
            try:
                host_2_hex(host, opts)
            except PxeConfig as detail:

                if opts.SKIP_HOSTNAME_LOOKUP_ERROR:
                    print('Skipping hostname lookup failed for: {}'.format(host))
                    continue
                else:
                    print(detail)
                    sys.exit(1)

def set_and_check_options(opts, file_defaults):
    """
    This  will set some global variables and check if dirs exists
    """
    global PXE_CONF_DIR
    global UEFI_CONF_DIR

    # debug can be specified by the command line or opts file
    #
    if not opts.DEBUG:
        try:
            if file_defaults['debug']:
                opts.DEBUG = int(file_defaults['debug'])
        except KeyError:
            pass

    try:
        PXE_CONF_DIR = file_defaults['pxe_config_dir']
    except KeyError:
        pass

    try:
        UEFI_CONF_DIR = file_defaults['uefi_config_dir']
    except KeyError:
        pass

    try:
        uefi_prefix = file_defaults['uefi_prefix']
    except KeyError:
        uefi_prefix = 'grub.cfg'
        pass

    try:
        pxe_prefix = file_defaults['pxe_prefix']
    except KeyError:
        pxe_prefix = 'default'
        pass

    if opts.UEFI_MODE:
        UEFI_CONF_DIR = os.path.realpath(UEFI_CONF_DIR)
        if not os.path.isdir(UEFI_CONF_DIR):
            error =  'uefi directory: {} does not exist'.format(UEFI_CONF_DIR)
            raise PxeConfig(error)
        opts.CONF_DIR = UEFI_CONF_DIR
        opts.prefix = uefi_prefix
    else:
        PXE_CONF_DIR = os.path.realpath(PXE_CONF_DIR)
        if not os.path.isdir(PXE_CONF_DIR):
            error =  'pxeconfig directory: {} does not exist'.format(PXE_CONF_DIR)
            raise PxeConfig(error)
        opts.CONF_DIR = PXE_CONF_DIR
        opts.prefix = pxe_prefix

    try:
        opts.SCRIPT_HOOK_ADD = file_defaults['client_script_hook_add']
    except KeyError as detail:
        pass

    try:
        opts.SCRIPT_HOOK_REMOVE = file_defaults['client_script_hook_remove']
    except KeyError as detail:
        pass

    if not opts.SKIP_HOSTNAME_LOOKUP_ERROR:
        try:
            opts.SKIP_HOSTNAME_LOOKUP_ERROR = file_defaults['skip_hostname_lookup_error']
        except KeyError as detail:
            pass

def main():
    parser = AdvancedParser.AdvancedParser(usage=__doc__)
    add_options(parser)
    opts, args = parser.parse_args()

    if opts.VERSION:
        print(VERSION)
        sys.exit(0)

    if not args:
        parser.print_help()
        sys.exit(0)

    if opts.DEBUG:
        print(args, opts)

    file_config, file_default_section = ReadConfig()

    set_and_check_options(opts, file_default_section)
    run(args, opts, file_config)

if __name__ == '__main__':
    try:
        main()
    except PxeConfig as detail:
        print(detail)
        sys.exit(1)
